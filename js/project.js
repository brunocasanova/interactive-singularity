
window.onload = function(){

  // store if it is already calibrated
  var calibrated = false;

  // Get elements
  var startBtn = $( '#start-btn' );
  var imageBack = $( '.image' );
  var calibrateBtn = $( '#calibrate-btn' );
  var doneBtn = $( '#done-btn' );

  // get window Width and Height
  var viewWidth = window.innerWidth;
  var viewHeight = window.innerHeight;

  // Get window width and height center
  var viewWidthCenter = parseInt( viewWidth / 2 );
  var viewHeightCenter = parseInt( viewHeight / 2 );

  // store video and face segments (overlay) options
  var largeX = Math.round( viewWidth / 4 );
  var largeY = Math.round( viewHeight / 3 );
  var radio = 30;
  var videoWidth = largeX - 2 * radio;
  var videoHeight = Math.round( videoWidth / ( 4 / 3 ) );
  var topDist = Math.round( viewHeight / 2.8 ) + 'px';
  var leftDist = Math.round( viewWidth / 2.55 ) + 'px';

  //////////////////////INITIALIZE////////////////////////////////////

  // inital elements setup
  imageBack.hide();
  calibrateBtn.hide();
  doneBtn.hide();

  // start tracking...
  setTimeout( checkIfReady, 100 );

  //////////////////////////////////////////////////////////

  // setup the video and overlay in the screen
  var setup = function() {
    // Clear cached prediction points
    window.localStorage.clear();
    
    // Get video and face overlay
    var video = document.getElementById( 'webgazerVideoFeed' );
    var overlay = document.createElement( 'canvas' );

    // video options
    video.style.display = 'block';
    video.style.position = 'absolute';
    video.style.top = topDist;
    video.style.left = leftDist;
    video.width = videoWidth;
    video.height = videoHeight;
    video.style.margin = '0px';
    webgazer.params.imgWidth = videoWidth;
    webgazer.params.imgHeight = videoHeight;
    
    // face segment options
    overlay.id = 'overlay';
    overlay.style.display = 'display';
    overlay.style.position = 'absolute';
    overlay.width = videoWidth;
    overlay.height = videoHeight;
    overlay.style.top = topDist;
    overlay.style.left = leftDist;
    overlay.style.margin = '0px';

    // append overlay options to element
    document.body.appendChild( overlay );

    // init face tracker
    var cl = webgazer.getTracker().clm;

    // face segment configs
    function drawLoop() {
      requestAnimFrame( drawLoop );

      overlay.getContext( '2d' ).clearRect( 0, 0, videoWidth, videoHeight );

      if( cl.getCurrentPosition() ){
        cl.draw( overlay );
      }
    }

    // draw segment lines in face
    drawLoop();
  };
  
  // start tracking
  function startTracking(){
    webgazer
      .setGazeListener( gazeListener )
      .showPredictionPoints( true )
      .resume();
  }

  // store coordinates
  var hitsCounter = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,

    topLeft: 0,
    topRight: 0,
    bottomLeft: 0,
    bottomRight: 0,
  };

  // count how many times its viewed
  function countHits( data ){

    // LEFT
    if( data.x < viewWidthCenter ){
      hitsCounter.left += 1;
    }

    // RIGHT
    if( data.x > viewWidthCenter ){
      hitsCounter.right += 1;
    }

    // TOP
    if( data.y < viewHeightCenter ){
      hitsCounter.top += 1;
    }

    // BOTTOM
    if( data.y > viewHeightCenter ){
      hitsCounter.bottom += 1;
    }

    // Top left
    if( data.y < viewHeightCenter && data.x < viewWidthCenter ){
      hitsCounter.topLeft += 1;
    }

    // Top Right
    if( data.y < viewHeightCenter && data.x > viewWidthCenter ){
      hitsCounter.topRight += 1;
    }

    // Bottom left
    if( data.y > viewHeightCenter && data.x < viewWidthCenter ){
      hitsCounter.bottomLeft += 1;
    }

    // Bottom Right
    if( data.y > viewHeightCenter && data.x > viewWidthCenter ){
      hitsCounter.bottomRight += 1;
    }

  }

  var moreTracked;
  var timeShift = 0;
  var realClock = 0;
  var choosen = undefined;

  var horizontal;
  var vertical;

  function gazeListener( data, clock ){

      if( data && calibrated ){
        
        // set the time after beeing calibrated
        if( timeShift < 1 ){
          timeShift = clock;
        }
        realClock = clock - timeShift;

        // Count the times gazed
        countHits( data );

        // Organize the hits
        horizontal = {
          left: hitsCounter.left,
          right: hitsCounter.right,
        };

        vertical = {
          top: hitsCounter.top,
          bottom: hitsCounter.bottom,
        };

        if( realClock > 1000 && realClock < 10100 ){

          if( data.x < viewWidthCenter ){
            imageBack.attr( 'src','img/1_big_pan_PANOPTIC.jpg' );

          }else if( data.x > viewWidthCenter ){
            imageBack.attr( 'src','img/1_big_pan_BIGBROTHER.jpg' );
          }

        }

        if( realClock > 10000 && realClock < 10100 ){

          moreTracked = getMaxArray( horizontal );

          if( moreTracked == 'left' ){
            imageBack.fadeOut(function (){
              choosen = 1;
              imageBack.attr( 'src','img/3_newton_satelite.jpg' ).fadeIn( 'slow' );
            });
            
          }else if( moreTracked == 'right' ){
            imageBack.fadeOut(function (){
              choosen = 2;
              imageBack.attr( 'src','img/2_eye_surveillance.jpg' ).fadeIn( 'slow' );
            });
            
          }

          resetCounter();
        }

        if( realClock >11100 && realClock < 20100 ){

          if( choosen == 1 ){
            if( data.x < viewWidthCenter ){
              imageBack.attr( 'src','img/3_newton_satelite_NEWTON.jpg' );

            }else if( data.x > viewWidthCenter ){
              imageBack.attr( 'src','img/3_newton_satelite_SATELITE.jpg' );
            }
          }

          if( choosen == 2 ){
            if( data.x < viewWidthCenter ){
              imageBack.attr( 'src','img/2_eye_surveillance_SURVEILLANCE.jpg' );

            }else if( data.x > viewWidthCenter ){
              imageBack.attr( 'src','img/2_eye_surveillance_EYE.jpg' );
            }
          }

        }

        /*

        if( realClock > 10000 && realClock < 10100 ){

            moreTracked = getMaxArray( horizontal );

            alert( moreTracked )

            if( choosen == 1 ){

              if( moreTracked == 'left' ){
                imageBack.attr( 'src','img/page_1.1.png' );
                choosen == 1.1;
              }else if( moreTracked == 'right' ){
                imageBack.attr( 'src','img/page_1.2.png' );
                choosen == 1.2;
              }

            }else if( choosen == 2 ){

              if( moreTracked == 'left' ){
                imageBack.attr( 'src','img/page_2.1.png' );
                choosen == 2.1;
              }else if( moreTracked == 'right' ){
                imageBack.attr( 'src','img/page_2.2.png' );
                choosen == 2.2;
              }

            }

          resetCounter();

        }

        if( realClock > 15000 && realClock < 15100 ){

            moreTracked = getMaxArray( horizontal );

            alert( moreTracked )

            if( choosen == 1.1 ){

              if( moreTracked == 'left' ){
                imageBack.attr( 'src','img/page_1.1.1.png' );
              }else if( moreTracked == 'right' ){
                imageBack.attr( 'src','img/page_1.2.1.png' );
              }

            }else if( choosen == 1.2 ){

              if( moreTracked == 'left' ){
                imageBack.attr( 'src','img/page_2.1.1.png' );
              }else if( moreTracked == 'right' ){
                imageBack.attr( 'src','img/page_2.2.1.png' );
              }

            }else if( choosen == 2.1 ){

              if( moreTracked == 'left' ){
                imageBack.attr( 'src','img/page_2.1.1.png' );
              }else if( moreTracked == 'right' ){
                imageBack.attr( 'src','img/page_2.2.1.png' );
              }

            }else if( choosen == 2.2 ){

              if( moreTracked == 'left' ){
                imageBack.attr( 'src','img/page_2.1.1.png' );
              }else if( moreTracked == 'right' ){
                imageBack.attr( 'src','img/page_2.2.1.png' );
              }

            }

          resetCounter();

        }

        if( realClock > 20000 && realClock < 20100 ){
          alert( 'RESET' );

          resetCounter();
          timeShift = 0;

          imageBack.attr( 'src','img/page_0.png' );
        }

        //console.log({
          //topLeft: hitsCounter.topLeft,
          //topRight: hitsCounter.topRight,
          //bottomLeft: hitsCounter.bottomLeft,
          //bottomRight: hitsCounter.bottomRight,
        //});

        //console.log( hitsCounter );
        //console.log( realClock );

        */
      }
  }

  function resetCounter(){
    hitsCounter = {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,

      topLeft: 0,
      topRight: 0,
      bottomLeft: 0,
      bottomRight: 0,
    };
  }

  function getMaxArray(obj){
    return Object.keys(obj).reduce(function(a, b){
      return obj[a] > obj[b] ? a : b
    });
  }

  function resetPage(){
    location.reload();
  }

  function startCalibrate(){
    webgazer
      .setRegression( 'ridge' )
      .setTracker( 'clmtrackr' )
      .showPredictionPoints( true )
      .begin();
  }

  function stopTracking(){
    webgazer.end();
  }

  function pauseTracking(){
    webgazer.pause();
  }

  function setupDone(){
    $( '#webgazerVideoFeed' ).hide();
    $( 'canvas' ).hide();
  }

  // lets see if it is ready, if not retry
  function checkIfReady() {
    if( webgazer.isReady() ){
      setup();
    }else{
      setTimeout( checkIfReady, 100 );
    }
  }

  // Events Listeners
  startBtn.mousedown(function(e){
    startBtn.hide();
    calibrateBtn.show();
  });

  calibrateBtn.mousedown(function(e){
    calibrateBtn.hide();
    startCalibrate();

    setTimeout(function(){
      doneBtn.show();
    }, 5000);
  });

  doneBtn.mousedown(function(e){
    doneBtn.hide();
    setupDone();

    calibrated = true;

    imageBack.attr( 'src','img/1_big_pan.jpg' ).fadeIn( 'slow' );
    startTracking();
  });
  
};